const through = require('through2');
const PluginError = require('plugin-error');
const path = require('path');
const camelcase = require('camelcase');
const prettier = require('prettier');
const Vinyl = require('vinyl');
const Svgo = require('svgo');

const PLUGIN_NAME = 'gulp-svg-vue';

async function prettify(content, parser) {
  const prettierConfigPath = await prettier.resolveConfigFile();
  const prettierConfig = await prettier.resolveConfig(prettierConfigPath);
  return prettier.format(content, {
    ...prettierConfig,
    parser
  });
}

async function createFile(content, svgo) {
  const optimizedSvg = svgo
    ? (await svgo.optimize(content)).data.toString()
    : content;

  // language=Vue
  const newFileContent = `
    <template>
      ${optimizedSvg}
    </template>
  `;

  return prettify(newFileContent, 'vue');
}

function getDefaultIcon(name, icons) {
  return icons[icons.length - 1];
}

async function createAggregatedFile(name, icons, options) {
  const aggregation = options.aggregate[0];

  const defaultIconFn = options.aggregateDefault || getDefaultIcon;
  const defaultIcon = defaultIconFn(name, icons);

  const renderIconImport = ([, componentName]) => {
    // language=js
    return `import ${componentName} from './${componentName}.vue'`;
  };

  const renderIconCase = ([variant, componentName]) => {
    return `
      case '${variant}': return ${componentName};
    `;
  };

  // language=Vue
  const content = `
    <template>
        <component :is="icon"/>
    </template>
    
    <script>
        ${icons.map(renderIconImport).join('\n')}
        export default {
          name: '${name}',
          components: {
            ${icons.map((icon) => icon[1]).join(',\n')}
          },
          props: {
            ${aggregation}: {
              type: String,
              default: ''
            }
          },
          computed: {
            icon() {
              switch (this.${aggregation}) {
                ${icons.map(renderIconCase).join('\n')}
                default: return ${
                  defaultIcon && defaultIcon[1] ? defaultIcon[1] : 'null'
                };
              }
            }
          }
        };
    </script>
  `;

  return prettify(content, 'vue');
}

async function createAllIconsFile(icons, extension) {
  const content =
    icons
      .map((icon) => `export { default as ${icon.name} } from './${icon.path}/${icon.name}.${extension}';`)
      .join('\n');

  return prettify(content, 'babel');
}

module.exports = function gulpSvgVue(options) {
  const {
    prefix = 'Icon',
    aggregate,
    createIndex,
    extension = 'vue',
    svgoConfig,
    preprocessFn
  } = options;

  const allIcons = [];
  const baseIconsMap = {};

  if (aggregate && !Array.isArray(aggregate)) {
    throw new PluginError(PLUGIN_NAME, "'aggregate' should be an array");
  }
  if (aggregate && aggregate.length > 1) {
    throw new PluginError(
      PLUGIN_NAME,
      `Aggregating by multiple dimensions is not supported. Got '${aggregate.join(
        ','
      )}'`
    );
  }

  const svgo = svgoConfig ? new Svgo(svgoConfig) : null;

  return through.obj(
    async function(file, encoding, callback) {
      if (file.isNull()) {
        return callback(null, file);
      }

      if (file.isStream()) {
        this.emit(
          'error',
          new PluginError(PLUGIN_NAME, 'Streams not supported!')
        );
      } else if (file.isBuffer()) {
        const fileContent = file.contents.toString();
        const iconPath = file.relative.split(/[\/\\]/);
        iconPath[iconPath.length - 1] = file.stem;
        iconPath.unshift(prefix || '');
        const iconName = camelcase(iconPath, { pascalCase: true });
        const basePath = iconPath.slice(0, iconPath.length - 1);

        if (aggregate) {
          // Group icons by aggregator
          const baseIcon = camelcase(basePath, { pascalCase: true });
          if (!baseIconsMap[baseIcon]) {
            baseIconsMap[baseIcon] = {
              path: basePath.slice(1),
              icons: []
            };
          }
          baseIconsMap[baseIcon].icons.push([file.stem, iconName]);
        }

        const preprocessedContent = preprocessFn ? await preprocessFn(fileContent) : fileContent
        const code = await createFile(preprocessedContent, svgo);

        const newFile = file.clone({ contents: false });
        newFile.contents = Buffer.from(code, 'utf-8');
        newFile.path = path.join(
          path.dirname(file.path),
          `${iconName}.${extension}`
        );
        this.push(newFile);

        allIcons.push({
          name: iconName,
          path: basePath.slice(1).join('/')
        });

        callback(null);
      } else {
        this.emit('error', new PluginError(PLUGIN_NAME, 'Unknown file type'));
      }
    },
    async function(cb) {
      if (aggregate) {
        // Do not save normal files
        allIcons.length = 0;

        // Save aggregated files
        for (const [iconName, info] of Object.entries(baseIconsMap)) {
          const combined = await createAggregatedFile(
            iconName,
            info.icons,
            options
          );
          const filename = `${iconName}.${extension}`;
          const newFile = new Vinyl({
            cwd: '',
            base: null,
            path: path.join(...info.path, filename),
            contents: Buffer.from(combined, 'utf-8')
          });
          this.push(newFile);
          allIcons.push({
            name: iconName,
            path: info.path.join('/')
          });
        }
      }

      if (createIndex) {
        allIcons.sort((a, b) => {
          return a.name < b.name ? -1 : a.name > b.name ? 1 : 0;
        });

        // Создаем общий файл со списком всех иконок
        const allIconsCombined = await createAllIconsFile(allIcons, extension);
        this.push(
          new Vinyl({
            cwd: '',
            base: null,
            path: typeof createIndex === 'string' ? createIndex : 'index.js',
            contents: Buffer.from(allIconsCombined, 'utf-8')
          })
        );
      }

      cb();
    }
  );
};
