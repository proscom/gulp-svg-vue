# `@proscom/gulp-svg-vue`

This is a [gulp](http://github.com/gulpjs/gulp) plugin which allows you to convert svg to vue components as a gulp task.

In current state this plugin is not generally intended for the general use as it relies on the couple of use-case
specific assumptions.

## Installation

```shell script
npm install --save-dev @proscom/gulp-svg-vue
# or
yarn add --dev @proscom/gulp-svg-vue
```

## Usage

In your `gulpfile.js` add this module as one of the transforms applied to your files:

```js
const gulpSvgr = require('@proscom/gulp-svg-vue');

function buildIcons() {
  return src('src/assets/icons/**/*.svg')
    .pipe(
      gulpSvgr({
        // To aggregate icons, pass an array (see below)
        aggregate: ['size'],
        // You can pass a function to determine which
        // icon to render when no aggregation matches props.
        // Default behavior is to render the last icon
        aggregateDefault: (name, icons) => icons[icons.length - 1],
        // Creates index.jsx file containing all the icons
        createIndex: true,
        // Or pass the filename of the index file
        createIndex: 'index.ts',
        // Icon file extension can be overridden
        extension: 'vue',
        // Icon file prefix can be overridden
        // Default prefix is 'Icon'
        prefix: 'MyIcon',
        // Preprocess function (see for example below)
        preprocessFn: content => content
      })
    )
    .pipe(dest('src/icons'));
}

module.exports.buildIcons = buildIcons;
```

### Aggregating icons

This plugin is capable of aggregating icons together. This is useful in cases when the icon has some variability, e.g.:

```
/back/small.svg
/back/medium.svg
/back/large.svg
``` 

Then this plugin will aggregate all these icons together, so you can dynamically choose the right variant:

```vue

<template>
  <IconBack size='small'></IconBack>
</template>

<script>
import { IconBack } from './icons';

export default {
  components: {
    IconBack
  }
};
</script>
```

Otherwise, you would have to import icons separately:

```vue

<template>
  <IconBackSmall></IconBackSmall>
</template>

<script>
import { IconBackSmall, IconBackMedium, IconBackLarge } from './icons';

export default {
  components: {
    IconBackSmall,
    IconBackMedium,
    IconBackLarge
  }
};
</script>
```

To aggregate icons, pass `aggregate` prop. It should be a one-item array containing the name of the aggregation
dimension. It will also be used as the prop of the resulting component which determines which icon to use.

### Preprocessing icons

You can modify icon content by passing function to `preprocessFn` property. `preprocessFn` is called with 1 argument,
that contains file content. `preprocessFn` is called before `svgo` and `prettier`. `preprocessFn` must return modified
file content.

For example, we need to replace `width` and `height` with `1em`:

```js
// gulpfile.js
const gulpSvgVue = require('@proscom/gulp-svg-vue');
// <...>
gulpSvgVue({
  aggregate: ['size'],
  createIndex: 'index.js',
  preprocessFn: (svgContent) => {
    const widthRegex = /width="\d+"/gm;
    const heightRegex = /height="\d+"/gm;
    return svgContent
      .replace(widthRegex, 'width="1em"')
      .replace(heightRegex, 'height="1em"')
  }
})
// <...>
```